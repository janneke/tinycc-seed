#! /bin/sh
# Mes --- Maxwell Equations of Software
# Copyright © 2018 Jan Nieuwenhuizen <janneke@gnu.org>
#
# This file is part of Mes.
#
# Mes is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# Mes is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mes.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" = "--version" ]; then
    echo -n "tinycc-seed (Mes) "
    cat "${0%/*}"/VERSION
    exit 0
fi

MES_PREFIX=${MES_PREFIX-../mes}

cp $MES_PREFIX/lib/x86-mes/crt1.o x86-mes
cp $MES_PREFIX/lib/x86-mes/libc+tcc.o x86-mes
cp $MES_PREFIX/lib/x86-mes/libc+gnu.o x86-mes
cp $MES_PREFIX/lib/x86-mes/libgetopt.o x86-mes

cp $MES_PREFIX/lib/x86-mes-gcc/crt1.o x86-mes-gcc
cp $MES_PREFIX/lib/x86-mes-gcc/libc+tcc.o x86-mes-gcc
cp $MES_PREFIX/lib/x86-mes-gcc/libtcc1.o x86-mes-gcc
cp $MES_PREFIX/lib/x86-mes-gcc/libc+gnu.o x86-mes-gcc
cp $MES_PREFIX/lib/x86-mes-gcc/libg.o x86-mes-gcc
cp $MES_PREFIX/lib/x86-mes-gcc/libgetopt.o x86-mes-gcc

cp $MES_PREFIX/lib/x86-mes-tcc/crt0.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/crt1.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/crti.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/crtn.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/libc+tcc.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/libtcc1.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/libc+gnu.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/libg.o x86-mes-tcc
cp $MES_PREFIX/lib/x86-mes-tcc/libgetopt.o x86-mes-tcc

cp $MES_PREFIX/lib/x86_64-mes-gcc/crt0.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/crt1.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/crti.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/crtn.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/libc+tcc.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/libtcc1.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/libc+gnu.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/libg.o x86_64-mes-gcc
cp $MES_PREFIX/lib/x86_64-mes-gcc/libgetopt.o x86_64-mes-gcc
